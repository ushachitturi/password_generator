import tkinter as tk
import random
import string

class PasswordGeneratorApp:
    def _init_(self, root):
        self.root = root
        self.root.title("PASSGENIE")
        self.root.geometry("300x300+750+250")

        self.label_strength = tk.Label(root, text="PASSWORD STRENGTH:")
        self.label_strength.pack()

        self.strength_var = tk.StringVar()
        self.strength_var.set("strong")

        self.radio_strong = tk.Radiobutton(root, text="Strong", variable=self.strength_var, value="strong")
        self.radio_strong.pack()

        self.radio_medium = tk.Radiobutton(root, text="Medium", variable=self.strength_var, value="medium")
        self.radio_medium.pack()

        self.radio_easy = tk.Radiobutton(root, text="Easy", variable=self.strength_var, value="easy")
        self.radio_easy.pack()

        self.label_length = tk.Label(root, text="PASSWORD LENGTH:")
        self.label_length.pack()

        self.password_length = tk.Entry(root)
        self.password_length.pack()

        self.generate_button = tk.Button(root, text="GET MY PASSWORD", command=self.generate_password)
        self.generate_button.pack()
        self.generated_password = tk.Label(root, text="")
        self.generated_password.pack()


    def generate_password(self):
        strength = self.strength_var.get()
        length = int(self.password_length.get())

        if strength == "strong":
            characters = string.ascii_letters + string.digits + string.punctuation
        elif strength == "medium":
            characters = string.ascii_letters + string.digits
        else:
            characters = string.ascii_lowercase

        password = ''.join(random.choice(characters) for _ in range(length))
    
        self.generated_password.config(text="GENERATED PASSWORD : " + password)


        

if _name_ == "_main_":
    root = tk.Tk()
    app = PasswordGeneratorApp(root)
    root.mainloop()